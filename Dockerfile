# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8-slim-buster

EXPOSE 5000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# Install ffmpeg and poetry
RUN apt update \
    && apt install -y --no-install-recommends wget tar xz-utils \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir ffmpeg-temp && cd ffmpeg-temp \
    && wget https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-amd64-static.tar.xz \
    && pwd && ls \
    && tar xvf ffmpeg-git-amd64-static.tar.xz --strip-components=1 \
    && mv ./ffmpeg ./ffprobe /usr/bin/ \
    && cd / && rm -rf /ffmpeg-temp \
    && pip install poetry

RUN useradd appuser \
    && mkdir /home/appuser \
    && mkdir /home/appuser/uarua \
    && chown -R appuser /home/appuser

USER appuser
WORKDIR /home/appuser/uarua

# Install pip requirements
ADD pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.in-project true \
    && poetry install --no-dev --no-root --no-interaction --no-ansi

ADD . .

RUN poetry install --no-dev --no-interaction --no-ansi

ENV VIRTUAL_ENV=/home/appuser/uarua/.venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["gunicorn", "--bind", "0.0.0.0:5000", "uarua.app:create_app()"]
