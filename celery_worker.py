from uarua.app import create_app
from uarua.tasks import celery

app = create_app()
app.app_context().push()


if __name__ == "__main__":
    from celery.bin import worker

    worker = worker.worker(app=celery)

    options = {
        # 'broker': 'amqp://guest:guest@localhost:5672//',
        "loglevel": "INFO",
        "traceback": True,
    }

    worker.run(**options)
