function eventDone(event_id) {
    $.ajax({
        type: 'POST',
        url: '/action/event/close',
        data: JSON.stringify({ 'event': event_id }),
        contentType: "application/json",
        dataType: 'json',
        success: function () {
            window.location.reload()
        }
    })
}
