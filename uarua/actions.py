import os
import signal
from datetime import datetime
from subprocess import Popen
from uuid import uuid4

from celery import chain
from flask import Blueprint, abort, current_app, redirect, request, url_for
from flask.views import MethodView
from flask_security import auth_required
from sqlalchemy import and_

from uarua.ext import db, r
from uarua.models import Destination, Event, Source
from uarua.tasks import get_source_url, push_stream

action = Blueprint("action", __name__)


@action.route("/publish/event/<int:event_id>", endpoint="publish-event")
@auth_required()
def publish_event(event_id):
    event = Event.query.get_or_404(event_id)
    if event.can_stream:
        destinations = Destination.query.filter(
            and_(Destination.event_id == event_id, Destination.deleted_at == None)
        ).all()

        for destination in destinations:
            if not r.exists(f"destination_live:{destination.id}"):
                push_stream.apply_async(
                    args=[
                        event.source.id,
                        event.source.url,
                        destination.id,
                        destination.url,
                        event.id,
                    ],
                    expires=15,
                )
                r.set(f"destination_queued:{destination.id}", f"{event_id}", ex=10)
    return redirect(url_for("event.detail", event_id=event_id))


@action.route(
    "/publish/event/<int:event_id>/destination/<int:destination_id>",
    endpoint="publish-destination",
)
@auth_required()
def publish_destination(event_id, destination_id):
    if r.exists(f"destination_live:{destination_id}"):
        abort(409)

    event = Event.query.get_or_404(event_id)
    if event.can_stream:
        r.set(f"desdination_queued:{destination_id}", f"{event_id}", ex=30)
        destination = Destination.query.get_or_404(destination_id)
        task = chain(
            get_source_url.s(event.source.url, event.source.is_external),
            push_stream.s(
                destination.url, destination.id, external=event.source.is_external
            ),
        ).apply_async(expires=30)
        """task = push_stream.delay(
            event.source.id,
            event.source.url,
            destination.id,
            destination.url,
            event.id,
        )"""
    return redirect(url_for("event.detail", event_id=event_id))


@action.route(
    "/stop/event/<int:event_id>/destination/<int:destination_id>",
    endpoint="stop-destination",
)
@auth_required()
def publish_stop_destination(event_id, destination_id):
    live = r.get(f"destination_live:{destination_id}")
    current_app.logger.info(f"stop>> live: {live}\n")
    if live:
        r.delete(f"destination_live:{destination_id}")
    return redirect(url_for("event.detail", event_id=event_id))


@action.route("/event/close", methods=["POST"])
@auth_required()
def event_close():
    data = request.get_json()

    if "event" in data:
        event = Event.query.get_or_404(int(data["event"]))
        event.ended_at = datetime.utcnow()
        db.session.commit()
        return "", 204
    abort(500)
