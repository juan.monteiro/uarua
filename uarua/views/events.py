from uuid import uuid4

from flask import Blueprint, current_app, redirect, render_template, url_for
from flask_security import auth_required, current_user
from sqlalchemy import and_, desc
from sqlalchemy.orm import joinedload, selectinload, subqueryload

from uarua.ext import db
from uarua.forms import EventForm
from uarua.models import Destination, Event, Source

event = Blueprint("event", __name__)


@event.route("/<int:event_id>")
@auth_required()
def detail(event_id):
    ctx = {
        "event": Event.query.get_or_404(event_id),
        "destinations": Destination.query.filter(
            and_(Destination.event_id == event_id, Destination.deleted_at == None)
        ).all(),
        "rtmp_server": f"{current_app.config['RTMP_SERVER']}live",
    }
    return render_template("event.html", **ctx)


@event.route("/list")
@auth_required()
def list_():
    ctx = {"events": Event.query.order_by(desc(Event.created_at)).all()}
    return render_template("event.list.html", **ctx)


@event.route("/new", methods=["GET", "POST"])
@auth_required()
def new():
    form = EventForm()
    if form.validate_on_submit():
        source = Source(title=f"source for {form.title.data}", stream_key=f"{uuid4()}")
        event = Event(
            title=form.title.data,
            description=form.description.data,
            source=source,
            owner=current_user,
        )
        db.session.add(event)
        if form.is_external.data:
            event.source.is_external = form.is_external.data
            event.source.external_url = form.external_url.data
        db.session.commit()
        return redirect(url_for("event.detail", event_id=event.id))
    return render_template("event.new.html", form=form)
