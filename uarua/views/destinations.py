from datetime import datetime

from flask import Blueprint, redirect, render_template, request, url_for
from flask_security import auth_required

from uarua.ext import db
from uarua.forms import DestinationForm
from uarua.models import Destination, Event

destination = Blueprint("destination", __name__)


@destination.route("/new/event/<int:event_id>", methods=["GET", "POST"])
@auth_required()
def new(event_id):
    form = DestinationForm()
    if form.validate_on_submit():
        event = Event.query.get_or_404(event_id)
        dest = Destination(
            title=form.title.data,
            stream_url=form.stream_url.data,
            stream_key=form.stream_key.data,
        )
        event.destinations.append(dest)
        db.session.commit()
        return redirect(url_for("event.detail", event_id=event_id))
    return render_template("destination.new.html", form=form)


@destination.route("/delete/<int:destination_id>")
@auth_required()
def delete(destination_id):
    destination = Destination.query.get_or_404(destination_id)
    destination.deleted_at = datetime.utcnow()
    db.session.commit()
    return redirect(request.referrer)
