from flask import Blueprint, render_template
from flask_security import auth_required, current_user

from uarua.models import Event
from uarua.ext import r

dashboard = Blueprint("dashboard", __name__)


@dashboard.route("/")
@auth_required()
def home():
    counter = r.get("live_counter")
    ctx = {
        "events": Event.query.filter(Event.ended_at == None),
        "live": counter.decode("utf-8") if counter else 0,
    }
    return render_template("dashboard.html", **ctx)
