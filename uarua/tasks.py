from datetime import datetime
from subprocess import DEVNULL, STDOUT
from time import sleep

from celery import states
from celery.exceptions import Ignore

from flask import current_app
from gevent.subprocess import Popen, TimeoutExpired, sleep, check_output

from uarua.app import create_celery
from uarua.ext import r, mail

celery = create_celery()


@celery.task(bind=True)
def get_source_url(self, url: str, external: bool):
    """Tenta encontrar a URL da fonte externa com Streamlink e youtube-dl"""
    if not external:
        return url

    streamlink = (
        check_output(
            [
                "streamlink",
                url,
                "--stream-url",
                "--default-stream",
                "720p,720p60,best",
            ],
            stderr=STDOUT,
        )
        .decode("ascii")
        .strip()
    )

    if streamlink[0:5] != "error":
        return streamlink

    self.update_state(state=states.FAILURE, meta=streamlink)
    raise Ignore()


@celery.task(bind=True)
def push_stream(self, source, destination, destination_id, external=False):
    if r.get(f"destination_live:{destination_id}"):
        self.update_state(state=states.FAILURE, meta="Destination occupied.")
        raise Ignore()

    encoding = ["-codec", "copy"]
    if external:
        encoding = [
            "-c:v",
            "libx264",
            "-preset",
            "ultrafast",
            "-tune",
            "fastdecode",
            "-x264-params",
            "nal-hrd=cbr",
            "-b:v",
            "3M",
            "-minrate",
            "3M",
            "-maxrate",
            "3M",
            "-bufsize",
            "2",
            "-c:a",
            "aac",
            "-ar",
            "44100",
            "-ab",
            "128k",
            "-ac",
            "2",
            "-strict",
            "-2",
            "-flags",
            "+global_header",
            "-bsf:a",
            "aac_adtstoasc",
            "-bufsize",
            "3000k",
        ]

    ffmpeg = Popen(
        [
            "/usr/bin/ffmpeg",
            "-re",
            "-report",
            "-nostdin",
            "-i",
            source,
            *encoding,
            "-f",
            "flv",
            destination,
        ],
        stdout=DEVNULL,
        stderr=DEVNULL,
    )
    r.set(f"destination_live:{destination_id}", f"{source}", ex=30)
    while ffmpeg.poll() is None:
        status = r.get(f"destination_live:{destination_id}")
        if not status or status == "stop":
            ffmpeg.terminate()
            r.delete(f"destination_live:{destination_id}")
            break
        r.expire(f"destination_live:{destination_id}", 10)
        sleep(5)

    return "Done"
