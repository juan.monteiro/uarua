from datetime import datetime, timedelta

from celery import Celery
from flask import Flask, Response, abort, render_template, request
from flask_mail import Message
from flask_security import Security, SQLAlchemyUserDatastore, hash_password

from uarua import config
from uarua.ext import db, r, migrate, mail, celery


def create_app(blueprints=True):
    app = Flask(__name__)
    app.config.from_object(config)

    db.init_app(app)
    migrate.init_app(app, db)
    # celery.conf.update(app.config)
    r.init_app(app)
    mail.init_app(app)

    from uarua.models import Event, Role, Source, User, Destination

    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security = Security(app, user_datastore)

    if blueprints:
        register_blueprints(app)

    @app.cli.command("createdb")
    def reset():
        db.create_all()

    @app.cli.command("dropdb")
    def reset():
        db.drop_all()

    @app.errorhandler(404)
    def error_404(error):
        return render_template("404.html"), 404

    @security.send_mail_task
    def delay_flask_security_mail(msg):
        send_flask_mail.delay(
            subject=msg.subject,
            sender=msg.sender,
            recipients=msg.recipients,
            body=msg.body,
            html=msg.html,
        )

    return app


@celery.task
def send_flask_mail(**kwargs):
    mail.send(Message(**kwargs))


def create_celery(app=None):
    app = app or create_app(blueprints=False)
    celery.conf.update(app.config)

    class ContextTask(celery.Task):  # noqa
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


def register_blueprints(app):
    # with app.app_context():
    from uarua.actions import action
    from uarua.rtmp import bp as nginx_rtmp_blueprint
    from uarua.views.events import event
    from uarua.views.destinations import destination
    from uarua.views.dashboard import dashboard

    app.register_blueprint(dashboard)
    app.register_blueprint(event, url_prefix="/event")
    app.register_blueprint(destination, url_prefix="/destination")
    app.register_blueprint(action, url_prefix="/action")
    app.register_blueprint(nginx_rtmp_blueprint, url_prefix="/nginx_rtmp")
