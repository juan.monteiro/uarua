from celery import Celery
from flask_mail import Mail
from flask_migrate import Migrate
from flask_redis import FlaskRedis
from flask_sqlalchemy import SQLAlchemy

from uarua import config

r = FlaskRedis()
db = SQLAlchemy()
migrate = Migrate()
mail = Mail()
celery = Celery(
    __name__, broker=config.CELERY_BROKER_URL, backend=config.CELERY_RESULT_BACKEND
)
