from datetime import datetime
from flask import current_app
from flask_security.models import fsqla_v2
from uarua.ext import db, r


fsqla_v2.FsModels.set_db_info(db)


class Role(db.Model, fsqla_v2.FsRoleMixin):
    pass


class User(db.Model, fsqla_v2.FsUserMixin):
    pass


class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.ForeignKey("user.id"))
    title = db.Column(db.String(40), nullable=False)
    description = db.Column(db.String(255), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, default=None)

    start_at = db.Column(db.DateTime)
    ended_at = db.Column(db.DateTime)

    source_id = db.Column(db.ForeignKey("source.id"))
    source = db.relationship("Source", order_by="Source.created_at")

    owner = db.relationship("User", lazy="noload")
    destinations = db.relationship("Destination", lazy="noload")

    @property
    def can_stream(self):
        if self.is_done is False or self.source.is_external is True:
            return True
        return False

    @property
    def is_done(self):
        if self.deleted_at is None or self.ended_at is None:
            return False
        return True


class Source(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, default=None)

    title = db.Column(db.String(2048))
    stream_key = db.Column(db.String(36), nullable=False)
    is_external = db.Column(db.Boolean(), default=False)
    external_url = db.Column(db.String(1024))

    @property
    def url(self):  # TODO: fix source url
        if self.is_external:
            return self.external_url
        return f"{current_app.config['RTMP_SERVER']}live/{self.stream_key}"

    @property
    def preview_url(self):  # TODO: fix source url
        return f"{current_app.config['PREVIEW_SERVER']}/{self.stream_key}.m3u8"

    @property
    def is_live(self):
        if r.exists(f"source_live:{self.url}"):
            return True
        return False


class Destination(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    deleted_at = db.Column(db.DateTime, default=None)

    title = db.Column(db.String(36), nullable=False)
    event_id = db.Column(db.ForeignKey("event.id"), nullable=False)
    stream_url = db.Column(db.String(256), nullable=False)
    stream_key = db.Column(db.String(256))

    @property
    def url(self):
        return f"{self.stream_url}/{self.stream_key}"

    @property
    def is_live(self):
        if r.exists(f"destination_live:{self.id}"):
            return True
        return False

    @property
    def is_queued(self):
        if r.exists(f"desdination_queued:{self.id}"):
            return True
        return False
