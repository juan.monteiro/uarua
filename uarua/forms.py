from flask_wtf import FlaskForm
from wtforms import StringField, DateTimeField, BooleanField
from wtforms.validators import DataRequired


class EventForm(FlaskForm):
    title = StringField("title", validators=[DataRequired()])
    description = StringField("description")
    start_at = DateTimeField("start_at")
    is_external = BooleanField("Fonte Externa")
    external_url = StringField("Fonte URL")


class DestinationForm(FlaskForm):
    title = StringField("title", validators=[DataRequired()])
    stream_url = StringField("stream_url", validators=[DataRequired()])
    stream_key = StringField("stream_key", validators=[DataRequired()])
