from datetime import datetime

from flask import Blueprint, abort, current_app, request

from uarua.ext import db, r
from uarua.models import Event, Source

bp = Blueprint("nginx_rtmp", __name__)


@bp.route("/callback/on_publish", methods=["POST"])
def on_publish():
    stream_key = request.form.get("name")
    if stream_key:
        stream = Source.query.filter_by(stream_key=stream_key).first_or_404()
        event = Event.query.get_or_404(stream.id)
        if stream_key and event.is_done is False:
            r.set(f"source_live:{stream_key}", str(datetime.utcnow()), ex=30)
            r.incr("live_counter")
            return {}
    abort(401)


@bp.route("/callback/on_play", methods=["POST"])
def on_play():
    destination_id = request.form.get("dest")
    if destination_id:
        r.expire(f"destination_live:{destination_id}", 30)
    return {}


@bp.route("/callback/on_update", methods=["POST"])
def on_update():
    call = request.form.get("call")
    if call == "update_publish":
        stream_key = request.form.get("name")
        if stream_key:
            r.set(f"source_live:{stream_key}", 15)
            r.expire("live_counter", 15)

    if call == "update_play":
        destination_id = request.form.get("dest")
        current_app.logger.info(f"update_play>> destination {destination_id}")
        if destination_id:
            r.expire(f"destination_live:{destination_id}", 15)

    return request.form


@bp.route("/callback/on_publish_done", methods=["POST"])
def on_publish_done():
    stream_key = request.form.get("name")
    if stream_key:
        r.delete(f"source_live:{stream_key}")
        r.decr("live_counter")
    return {}


@bp.route("/callback/on_play_done", methods=["POST"])
def on_play_done():
    destination_id = request.form.get("dest")
    if destination_id:
        r.delete(f"destination_live:{destination_id}")
    return {}
